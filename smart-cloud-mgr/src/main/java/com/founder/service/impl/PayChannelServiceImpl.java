package com.founder.service.impl;

import com.founder.core.dao.PayChannelRespository;
import com.founder.core.domain.PayChannel;
import com.founder.core.log.MyLog;
import com.founder.service.IPayChannelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

@Service
public class PayChannelServiceImpl implements IPayChannelService {

    private static final MyLog _log = MyLog.getLog(PayChannelServiceImpl.class);

    @Autowired
    PayChannelRespository payChannelRespository;

    @Override
    public PayChannel selectPayChannel(int id) {
        return payChannelRespository.getOne(id);
    }

    @Override
    public PayChannel selectPayChannel(String channelId, String mchId) {
        return payChannelRespository.findByMchIdAndChannelId(mchId,channelId);
    }

    @Override
    public Page<PayChannel> selectPayChannelList(int offset, int limit, PayChannel payChannel) {
        _log.info("分页查询支付渠道列表，offset={}，limit={}。", offset, limit);
        Pageable pageable = PageRequest.of(offset, limit);
        Page<PayChannel> page = payChannelRespository.findAll(new Specification<PayChannel>() {
            @Override
            public Predicate toPredicate(Root<PayChannel> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = criteriaBuilder.conjunction();
                if (payChannel != null){
                    String mchId = payChannel.getMchId();
                    if (StringUtils.isBlank(mchId)){
                        _log.info("传入商户号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchId"), "%"));
                    } else {
                        _log.info("按照商户号模糊查询支付渠道");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchId"), "%"+payChannel.getMchId().trim()+"%"));
                    }
                    String channelId = payChannel.getChannelId();
                    if (StringUtils.isBlank(channelId)){
                        _log.info("传入支付渠道号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("channelId"), "%"));
                    } else {
                        _log.info("按照渠道号模糊查询支付渠道");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("channelId"), "%"+payChannel.getChannelId().trim()+"%"));
                    }
                }
                criteriaQuery.where(predicate);
                criteriaQuery.orderBy(criteriaBuilder.asc(root.get("mchId").as(String.class)),
                        criteriaBuilder.asc(root.get("channelId").as(String.class)),
                        criteriaBuilder.desc(root.get("createTime").as(Date.class)));

                return criteriaQuery.getRestriction();
            }
        }, pageable);
        return page;
    }

    @Override
    public Integer count(PayChannel payChannel) {
        Example<PayChannel> example = Example.of(payChannel);
        Long count = payChannelRespository.count(example);
        return count.intValue();
    }

    @Override
    public List<PayChannel> getPayChannelList(int offset, int limit, PayChannel payChannel) {
        Pageable pageable = PageRequest.of(offset,limit);
        Example<PayChannel> example = Example.of(payChannel);
        Page<PayChannel> page = payChannelRespository.findAll(example,pageable);
        return page.getContent();
    }

    @Override
    public int addPayChannel(PayChannel payChannel) {
        PayChannel channel = payChannelRespository.save(payChannel);
        return channel == null ? 0 : 1;
    }

    @Override
    public int updatePayChannel(PayChannel payChannel) {
        PayChannel channel = payChannelRespository.save(payChannel);
        return channel == null ? 0 : 1;
    }
}
